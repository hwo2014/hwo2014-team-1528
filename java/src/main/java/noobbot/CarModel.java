package noobbot;

import noobbot.model.json.TrackInfo;


public class CarModel {

	private int tick;
	
	private TrackInfo track;

	private int lap;
	private int pieceIndex;
	private double inPieceDistance;
	private double velocity;
	private double acceleration;
	private double angle;
	private double prevAngle;
	
	public CarModel(TrackInfo track) {
		this.track = track;
	}
	
	public int updatePosition(int lap, int pieceIndex, double inPieceDistance, double angle) {
		double prevVelocity = velocity;
		double prevDistance = getDistance();
		prevAngle = this.angle;
		this.angle = angle;
		this.lap = lap;
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		velocity = getDistance() - prevDistance;
		acceleration = velocity - prevVelocity;
		return ++tick;
	}

	public double getDistance() {
		double totalDistance = 0;
		for (int i=0; i<pieceIndex; ++i) {
			totalDistance += track.pieces.get(i).length;
		}
		totalDistance += inPieceDistance;			

		return totalDistance;
	}

	public double getAvgVelocity() {
		return getDistance() / tick;
	}

	public double getVelocity() {
		return velocity;
	}

	public double getAcceleration() {
		return acceleration;
	}

	public double getAngle() {
		return angle;
	}

    public double getAngleChange() {
        return angle - prevAngle;
    }

}
