
package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.msg.out.Join;
import noobbot.msg.out.JoinBase;
import noobbot.msg.out.JoinRace;

public class Main {
    public static final boolean LOG_DEBUG = false;
    public static final boolean EXIT_ON_CRASH = false;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String trackName;

        System.out.println("Connecting to " + host + ":" + port + " as "
                + botName + "/" + botKey);

        @SuppressWarnings("resource")
        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
                socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(
                socket.getInputStream(), "utf-8"));

        // TODO hardcoded
        int carCount = 1;
        if (args.length > 4) {
            trackName = args[4];
        } else {
            trackName = "keimola";
        }

        new JarkaBot(reader, writer).run(createJoinMessage(botName, botKey, trackName, carCount));
    }

    private static JoinBase createJoinMessage(String botName, String botKey, String trackName, int carCount) {
        if ("keimola".equals(trackName) && carCount == 1) {
            return new Join(botName, botKey);
        }
        return new JoinRace(botName, botKey, trackName, carCount);
    }

}
