
package noobbot;

public class ThrottleAngleContainer {
    private double angle;
    private double throttle;

    public ThrottleAngleContainer(double angle, double throttle) {
        this.angle = angle;
        this.throttle = throttle;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getThrottle() {
        return throttle;
    }

    public void setThrottle(double throttle) {
        this.throttle = throttle;
    }

    @Override
    public String toString() {
        return " angle " + angle + " throttle " + throttle;
    }
}
