
package noobbot;

import static noobbot.Main.LOG_DEBUG;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import noobbot.model.json.CarPosition;
import noobbot.model.json.Race;
import noobbot.model.json.TrackInfo;
import noobbot.model.json.TurboOption;
import noobbot.msg.in.CarPositions;
import noobbot.msg.out.SwitchLane;

public class LukaBot extends Bot {

    private CarModel car;

    private double throttle = 1.0;
    private int nextBendIndex = 0;
    private int bendIndexLimit = 0;
    private boolean pendingSwitch = false;
    private DecimalFormat df = new DecimalFormat();

    public LukaBot(BufferedReader reader, PrintWriter writer) {
        super(reader, writer);
    }

    @Override
    public void onSetup() {
        df.setMaximumFractionDigits(4);
        df.setMinimumFractionDigits(4);
    }

    @Override
    public void onRaceInit(Race race) {
        super.onRaceInit(race);

        TrackInfo track = getTrackInfo();
        bendIndexLimit = track.pieces.size() * race.getLapsQuantity();
        while (track.pieces.get(nextBendIndex).isStraight()) {
            nextBendIndex++;
        }
        car = new CarModel(track);
    }

    @Override
    public void onCarPositions(CarPositions message) {
        if (!isOnTrack()) {
            return;
        }
        throttle = 1.0;

        CarPosition myPosition = getMyPosition(message);
        TrackInfo track = getTrackInfo();
        int currentLap = myPosition.getCurrentLap();
        int currentPieceIndex = myPosition.getCurrentPieceIndex();
        double inPieceDistance = myPosition.getInPieceDistance();
        car.updatePosition(currentLap, currentPieceIndex, inPieceDistance, myPosition.getAngle());

        if (LOG_DEBUG) {
            String straightOrBend = getTrackPiece(currentPieceIndex).isBend() ? "b" : "s";
            System.out.println("tick: " + message.getTick() + "\tlap: " + currentLap
                    + "\tpiece: " + currentPieceIndex + " (" + straightOrBend + ")\ts: "
                    + df.format(car.getDistance()) + "\tv: " + df.format(car.getVelocity()) + "\ta: "
                    + df.format(car.getAcceleration()) + "\tangle: " + df.format(car.getAngle()) + "\t\u0394\u0391:"
                    + df.format(car.getAngleChange()));
        }

        if (nextBendIndex <= track.pieces.size()
                * currentLap
                + currentPieceIndex) {
            while (getTrackPiece(nextBendIndex).isBend()) {
                nextBendIndex++;
            }
            while (getTrackPiece(nextBendIndex).isStraight()) {
                nextBendIndex++;
            }
        }

        if (myPosition.getStartLaneIndex() != myPosition.getEndLaneIndex()) {
            pendingSwitch = false;
        }

        if (message.getTick() == 1) {
            send(new SwitchLane("Right"));
            pendingSwitch = true;
            return;
        }

        double maxVelocityInNextBend = 6.28;
        double toFast = car.getVelocity() - maxVelocityInNextBend;
        if (track.pieces.get(currentPieceIndex)
                .isBend()) {
            if (Math.signum(car.getAngle()) * Math.signum(car.getAngleChange()) < 1 && Math.abs(car.getAngle()) < 35
                    && Math.abs(car.getAngleChange()) < 3) {
                throttle = 1.0;
            } else if (toFast > 0.2 || Math.abs(myPosition.getAngle()) > 45) {
                throttle = car.getVelocity() / 12.3;
                if (getTrackPiece(currentPieceIndex).radius > 150) {
                    throttle *= 1.5;
                }
            } else {
                throttle = 0.79;
                if (inPieceDistance > track.pieces
                        .get(currentPieceIndex).length / 2) {
                    throttle = 0.95;
                }
            }
        } else {
            if (getTrackPiece(nextBendIndex).radius > 150) {
                maxVelocityInNextBend = 7;
                toFast = car.getVelocity() - maxVelocityInNextBend;
            }
            if (toFast > 0) {
                double timeToStop = toFast / 0.17;
                double distanceToStop = car.getVelocity()
                        * timeToStop - 0.17 / 2
                        * Math.pow(timeToStop, 2);
                double distanceToBend = -inPieceDistance;
                if (nextBendIndex >= bendIndexLimit) {
                    distanceToBend = Double.MAX_VALUE;
                } else {
                    for (int i = track.pieces.size()
                            * currentLap
                            + currentPieceIndex; i < nextBendIndex; i++) {
                        distanceToBend += getTrackPiece(i).length;
                    }
                    if (LOG_DEBUG) {
                        System.out.println("toFast: " + toFast
                                + ", timeToStop: " + timeToStop);
                        System.out.println("distance to stop: "
                                + distanceToStop);
                        System.out.println("distance to bend: "
                                + distanceToBend);
                    }

                    if (distanceToBend - distanceToStop <= 0) {
                        throttle = 0.0;
                    }
                }
            }
        }

        TurboOption turbo = getTurbo();
        if (turbo != null) {
            if (turbo.isAvailable()
                    && (getTrackPiece(currentPieceIndex).isStraight() || Math.signum(car.getAngle())
                            * Math.signum(car.getAngleChange()) < 1) && currentPieceIndex != 19) {
                double timeToStop = toFast / 0.17;
                double distanceToStop = car.getVelocity()
                        * timeToStop - 0.17 / 2
                        * Math.pow(timeToStop, 2);
                double distanceToBend = -inPieceDistance;
                if (nextBendIndex >= bendIndexLimit) {
                    distanceToBend = Double.MAX_VALUE;
                } else {
                    for (int i = track.pieces.size()
                            * currentLap
                            + currentPieceIndex; i < nextBendIndex; i++) {
                        distanceToBend += getTrackPiece(i).length;
                    }
                    if (LOG_DEBUG) {
                        System.out.println("## toFast: " + toFast
                                + ", timeToStop: " + timeToStop);
                        System.out.println("## distance to stop: "
                                + distanceToStop);
                        System.out.println("## distance to bend: "
                                + distanceToBend);
                    }
                }
                turbo.activate(message.getTick());
                send(TURBO_MSG);
                return;
            }
        }

        if (!pendingSwitch && currentPieceIndex == 4) {
            send(new SwitchLane("Left"));
            pendingSwitch = true;
            return;
        }
        if (!pendingSwitch && currentPieceIndex == 14) {
            send(new SwitchLane("Right"));
            pendingSwitch = true;
            return;
        }

        THROTTLE_MSG.setValue(Math.min(Math.max(throttle, 0.0), 1.0));
        send(THROTTLE_MSG);
    }

}
