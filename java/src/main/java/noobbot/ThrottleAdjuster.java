
package noobbot;

import java.util.HashMap;
import java.util.Map;

public class ThrottleAdjuster {
    HashMap<String, ThrottleAngleContainer> map;

    public ThrottleAdjuster() {
        map = new HashMap<>();
    }

    public void put(double throttle, double angle, PilotInfo pilotInfo) {
        if (map.get(pilotInfo.toString()) == null) {
            map.put(pilotInfo.toString(), new ThrottleAngleContainer(angle, throttle));
        } else if (map.get(pilotInfo.toString()).getAngle() < angle) {
            map.put(pilotInfo.toString(), new ThrottleAngleContainer(angle, throttle));
        }
    }

    public ThrottleAngleContainer get(PilotInfo pilotInfo) {
        return map.get(pilotInfo.toString());
    }

    public void print() {
        for (Map.Entry<String, ThrottleAngleContainer> entry : map.entrySet()) {

            System.out.println("Key : " + entry.getKey() + " Value : "
                    + entry.getValue());
        }
    }
}
