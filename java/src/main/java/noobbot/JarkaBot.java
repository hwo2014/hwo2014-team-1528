
package noobbot;


import java.io.BufferedReader;
import java.io.PrintWriter;

import noobbot.model.json.CarPosition;
import noobbot.model.json.Race;
import noobbot.model.json.TrackInfo;
import noobbot.msg.in.CarPositions;
import noobbot.msg.out.SwitchLane;

public class JarkaBot extends Bot {

    private PilotInfo pilotInfo;
    private CarCounter carCounter;
    private TrackAnalyser trackAnalyser;
    private ThrottleAdjuster throttleAdjuster;

    private double holdDistanceStart = 0;
    private double earlierAngle = 0;
    private double carDistance = 0;
    private double lastDistanceDelta = 0;
    private int currentLap = 0;
    private double throttle = 0.5;

    public JarkaBot(BufferedReader reader, PrintWriter writer) {
        super(reader, writer);
    }

    public void onSetup() {
        super.onSetup();
        trackAnalyser = new TrackAnalyser();
        pilotInfo = new PilotInfo();
        carCounter = new CarCounter();
        throttleAdjuster = new ThrottleAdjuster();
        pilotInfo.setStart(true);
    }

    public void onCarPositions(CarPositions positionsMessage) {
        if (positionsMessage.getTick() == 1) {
            send(new SwitchLane("Right"));
            return;
        }

        CarPosition carPosition = getMyPosition(positionsMessage);
        TrackInfo track = getTrackInfo();
        pilotInfo = trackAnalyser.getInfo(carCounter.getCarDistance(carPosition, track), currentLap);

        if (currentLap != carPosition.getCurrentLap()) {
            carDistance = 0;
            currentLap = carPosition.getCurrentLap();
        }

        if (pilotInfo != null) {
            if (pilotInfo.isStraight()) {
                throttle = 1;
            }

            double currentAngle = Math.abs(carPosition.getAngle());
            double carAngleDelta = currentAngle - earlierAngle;
            double bendRadius = track.pieces.get(carPosition.getCurrentPieceIndex()).radius;
            double carDistanceDelta = Math.max(carCounter.getCarDistance(carPosition, track) - carDistance, 0);

            if (Math.abs(carDistanceDelta - lastDistanceDelta) > 2) {
                carDistanceDelta = lastDistanceDelta;
            }

            lastDistanceDelta = carDistanceDelta;
            double lengthToFuture = Math.pow(carDistanceDelta, 2) + (pilotInfo.isBeCareful() ? 50 : 0)
                    + pilotInfo.getLength() / 10;
            System.out.println("length to future " + lengthToFuture + " be careful " + pilotInfo.isBeCareful());

            System.out.println("angle " + Math.abs(carPosition.getAngle()) + " angle delta " + carAngleDelta
                    + " carDistance delta " + carDistanceDelta);

            PilotInfo futurePilotInfo = trackAnalyser.getInfo(carCounter.getCarDistance(carPosition, track)
                    + lengthToFuture, currentLap);

            if (!futurePilotInfo.isStraight()) {
                if (currentLap > 0) {
                    ThrottleAngleContainer throttleAngleContainer = throttleAdjuster.get(pilotInfo);
                    ThrottleAngleContainer futureAngleContainer = throttleAdjuster.get(futurePilotInfo);

                    throttle = 0;

                    if (throttleAngleContainer.getAngle() < (50 * bendRadius) / 100d) {
                        if (holdDistanceStart == 0) {
                            holdDistanceStart = carDistance;
                        }

                        if (futureAngleContainer.getAngle() < (50 * bendRadius) / 100d) {
                            if (carDistance - holdDistanceStart < (25 * bendRadius) / 100 && carAngleDelta < 0) {
                                throttle = 1;
                                System.out.println("hold 30");
                            } else {
                                throttle = 0;
                            }
                        } else {
                            throttle = 0;
                        }

                        if (carAngleDelta < 0) {
                            holdDistanceStart = 0;
                        }
                    } else {
                        throttle = 0;
                    }
                } else {
                    throttle = 0;
                }
            }

            if (carDistanceDelta <= (6.5 * bendRadius) / 100d && carAngleDelta <= 0) {
                throttle = 1;
            }

            earlierAngle = Math.abs(carPosition.getAngle());
            carDistance = carCounter.getCarDistance(carPosition, track);

            if (currentLap == 0) {
                throttleAdjuster.put(throttle, earlierAngle, pilotInfo);
            }

            if (currentLap == 2 && carPosition.getCurrentPieceIndex() > 5
                    && futurePilotInfo.isStraight() && futurePilotInfo.getLength() > 500
                    && carAngleDelta < 0) {
                System.out.println("Turbo " + futurePilotInfo.getLength());
                send(TURBO_MSG);
            }
        }
        
        THROTTLE_MSG.setValue(throttle);
        send(THROTTLE_MSG);

        System.out.println("Throttle " + throttle);
    }

    @Override
    public void onRaceInit(Race race) {
        super.onRaceInit(race);
        trackAnalyser.analyse(getTrackInfo());        
    }
    
    @Override
    public void onRaceEnd() {
        throttleAdjuster.print();
        super.onRaceEnd();
    }
}
