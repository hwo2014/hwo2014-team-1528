
package noobbot;

public class RangePilotInfoContainer {
    private Range range;
    private PilotInfo pilotInfo;

    public RangePilotInfoContainer(Range range, PilotInfo pilotInfo) {
        this.range = range;
        this.pilotInfo = pilotInfo;
    }

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public PilotInfo getPilotInfo() {
        return pilotInfo;
    }

    public void setPilotInfo(PilotInfo pilotInfo) {
        this.pilotInfo = pilotInfo;
    }

}
