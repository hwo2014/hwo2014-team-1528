
package noobbot;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import noobbot.model.json.Piece;
import noobbot.model.json.TrackInfo;

public class TrackAnalyser {
    LinkedHashMap<Range, PilotInfo> trackMap = new LinkedHashMap<>();
    List<RangePilotInfoContainer> list;
    PilotInfo firstTrackPart;
    PilotInfo lastTrackPart;

    public TrackAnalyser() {
        list = new ArrayList<>();
    }

    public void analyse(TrackInfo trackInfo) {
        List<Piece> pieces = trackInfo.pieces;
        double allTrackLength = 0;

        for (int i = 0; i < pieces.size(); i++) {
            allTrackLength = allTrackLength + CarCounter.calculatePieceLength(pieces.get(i));
        }

        double distance = 0;
        int pieceIndex = 0;

        do {
            PilotInfo pilotInfo = sumTrackPart(pieces, pieceIndex);
            RangePilotInfoContainer rangePilotInfoContainer = new RangePilotInfoContainer(new Range(distance, distance
                    + pilotInfo.getLength()), pilotInfo);

            if (pilotInfo.getAngleSum() > 45) {
                if (list.size() > 0) {
                    RangePilotInfoContainer container = list.get(list.size() - 1);
                    PilotInfo fromArray = container.getPilotInfo();

                    fromArray.setBeCareful(true);
                    trackMap.put(container.getRange(), fromArray);
                }
            }

            distance = distance + pilotInfo.getLength();
            pieceIndex = pilotInfo.getNextTrackPartFirstPieceIndex();

            if (distance > allTrackLength) {
                pilotInfo.setBeCareful(true);
            }

            list.add(rangePilotInfoContainer);
            trackMap.put(rangePilotInfoContainer.getRange(), pilotInfo);
        } while (distance < allTrackLength);

        int index = 0;
        for (Map.Entry<Range, PilotInfo> entry : trackMap.entrySet()) {
            if (index == 0) {
                firstTrackPart = entry.getValue();
            }

            System.out.println("Key : " + entry.getKey() + " Value : "
                    + entry.getValue());

            lastTrackPart = entry.getValue();
        }
    }

    public PilotInfo getInfo(double distance, int lap) {
        PilotInfo result = null;
        System.out.println("car distance " + distance);
        for (Map.Entry<Range, PilotInfo> entry : trackMap.entrySet()) {
            Range range = entry.getKey();
            result = entry.getValue();

            if (range.getStart() < distance && range.getEnd() > distance) {
                result = entry.getValue();
                break;
            }
        }

        if (result != null && result.getNextTrackPartFirstPieceIndex() == firstTrackPart.getNextTrackPartFirstPieceIndex()
                && isTheSameKindOfTrackPart(firstTrackPart, lastTrackPart) && lap > 0) {
            System.out.println("last track part");
            return lastTrackPart;
        }

        System.out.println("not last");
        return result;
    }

    private PilotInfo sumTrackPart(List<Piece> pieces, int pieceIndex) {
        PilotInfo pilotInfo = new PilotInfo();
        Piece currentPiece = pieces.get(pieceIndex);
        double length = 0;
        double angleSum = 0;

        int index = pieceIndex;
        do {
            length = length + CarCounter.calculatePieceLength(getPiece(pieces, index));
            angleSum = angleSum + Math.abs(getPiece(pieces, index).angle);
            index++;
        } while (isTheSameKindOfPiece(currentPiece, getPiece(pieces, index)));

        length = length > 0 ? length : 0;
        pilotInfo.setLength(length);
        pilotInfo.setNextTrackPartFirstPieceIndex(index % pieces.size());
        pilotInfo.setAngleSum(angleSum);

        if (nextPieceIsBendLeft(pieces, index)) {
            pilotInfo.setNextTrackPartBendLeft(true);
        } else if (nextPieceIsBendRight(pieces, index)) {
            pilotInfo.setNextTrackPartBendRight(true);
        } else {
            pilotInfo.setNextTrackPartStraight(true);
        }

        if (currentPiece.isBendLeft()) {
            pilotInfo.setBendLeft(true);
        } else if (currentPiece.isBendRight()) {
            pilotInfo.setBendRight(true);
        } else {
            pilotInfo.setStraight(true);
        }

        return pilotInfo;
    }

    private boolean isTheSameKindOfTrackPart(PilotInfo firstPilotInfo, PilotInfo secondPilotInfo) {
        return firstPilotInfo.isBendLeft() && secondPilotInfo.isBendLeft() || firstPilotInfo.isBendRight()
                && secondPilotInfo.isBendRight() || firstPilotInfo.isStraight() && secondPilotInfo.isStraight();
    }

    private boolean isTheSameKindOfPiece(Piece firstPiece, Piece secondPiece) {
        return firstPiece.isStraight() && secondPiece.isStraight() || firstPiece.isBendLeft() && secondPiece.isBendLeft()
                || firstPiece.isBendRight() && secondPiece.isBendRight();
    }

    private boolean nextPieceIsBendLeft(List<Piece> pieces, int currentIndex) {
        return getNextPiece(pieces, currentIndex).isBendLeft();
    }

    private boolean nextPieceIsBendRight(List<Piece> pieces, int currentIndex) {
        return getNextPiece(pieces, currentIndex).isBendRight();
    }

    private Piece getPiece(List<Piece> pieces, int currentIndex) {
        return pieces.get((currentIndex) % pieces.size());
    }

    private Piece getNextPiece(List<Piece> pieces, int currentIndex) {
        return getPiece(pieces, currentIndex + 1);
    }
}
