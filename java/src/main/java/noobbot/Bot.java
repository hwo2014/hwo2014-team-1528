
package noobbot;

import static noobbot.Main.EXIT_ON_CRASH;
import static noobbot.Main.LOG_DEBUG;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import noobbot.model.json.CarId;
import noobbot.model.json.CarPosition;
import noobbot.model.json.Piece;
import noobbot.model.json.Race;
import noobbot.model.json.TrackInfo;
import noobbot.model.json.TurboOption;
import noobbot.msg.in.CarPositions;
import noobbot.msg.in.GameInit;
import noobbot.msg.in.InMsgBase;
import noobbot.msg.in.TurboAvailable;
import noobbot.msg.in.YourCar;
import noobbot.msg.out.JoinBase;
import noobbot.msg.out.OutMsgBase;
import noobbot.msg.out.Ping;
import noobbot.msg.out.Throttle;
import noobbot.msg.out.Turbo;

import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;

public class Bot {

    static final Ping PING_MESSAGE = new Ping();
    static final Throttle THROTTLE_MSG = new Throttle();
    static final Turbo TURBO_MSG = new Turbo();
    
    private PrintWriter writer;
    private BufferedReader reader;
    private boolean outOfTheTrack;
    private CarId myCar;
    private TurboOption turbo;
    private TrackInfo track;
    
    public Bot(BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public void run(JoinBase join) throws IOException {
        String line = null;

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        send(join);

        onSetup();

        while ((line = reader.readLine()) != null) {
            if (!outOfTheTrack && LOG_DEBUG) {
                System.out.println("< " + line);
            }

            final InMsgBase msgFromServer = mapper.readValue(line, InMsgBase.class);

            if (checkMessageId(msgFromServer, "carPositions")) {
                onCarPositions(CarPositions.fromJson(line));
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                turbo = TurboAvailable.parse(line);
            } else if (checkMessageId(msgFromServer, "join") || checkMessageId(msgFromServer, "joinRace")) {
                System.out.println("Joined");
            } else if (checkMessageId(msgFromServer, "gameInit")) {
                System.out.println("Race init");
                onRaceInit(GameInit.parse(line));
            } else if (checkMessageId(msgFromServer, "gameEnd")) {
                System.out.println("Race end");
                onRaceEnd();
            } else if (checkMessageId(msgFromServer, "gameStart")) {
                System.out.println("Race start");
                onRaceStart();
            } else if (checkMessageId(msgFromServer, "crash")) {
                System.out.println("Karramba! Crash :/");
                outOfTheTrack = true;
                onCrash();
            } else if (checkMessageId(msgFromServer, "spawn")) {
                System.out.println("Back on the track, lets go !");
                outOfTheTrack = false;
            } else if (checkMessageId(msgFromServer, "yourCar")) {
                myCar = YourCar.parse(line);
            } else {
                send(new Ping());
            }
        }
    }

    protected void send(final OutMsgBase message) {
        String json = message.toJson();
        if (LOG_DEBUG) {
            System.out.println("> " + json);
        }
        writer.println(json);
        writer.flush();
    }

    public boolean isOnTrack() {
        return !outOfTheTrack;
    }
    
    protected boolean checkMessageId(final InMsgBase msgFromServer, String messageId) {
        return messageId.equals(msgFromServer.msgType);
    }

    protected CarPosition getMyPosition(CarPositions positionsMessage) {
        return positionsMessage.getCarPosition(myCar);
    }

    protected TrackInfo getTrackInfo() {
        return track;
    }
    
    protected TurboOption getTurbo() {
        return turbo;
    }
    
    public void onSetup() {
    }
    
    public void onRaceInit(Race race) {
        track = race.getTrackInfo();
    }

    public void onRaceStart() {
    }
    
    public void onCrash() {
        if (EXIT_ON_CRASH) {
            throw new RuntimeException();
        }
    }
    
    public void onCarPositions(CarPositions fromJson) {
    }
    
    public void onRaceEnd() {
    }

    protected Piece getTrackPiece(int index) {
        return track.pieces.get(index % track.pieces.size());
    }

}
