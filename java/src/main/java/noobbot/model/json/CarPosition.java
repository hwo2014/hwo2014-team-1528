
package noobbot.model.json;

import org.codehaus.jackson.annotate.JsonProperty;

public class CarPosition {

    private static class PiecePosition {

        private static class LaneData {
            public int startLaneIndex;
            public int endLaneIndex;
        }

        public int pieceIndex;
        public double inPieceDistance;
        public int lap;

        public LaneData lane;

    }

    @JsonProperty("id")
    private CarId carId;
    private double angle;
    @JsonProperty
    private PiecePosition piecePosition;
    
    public int getCurrentLap() {
        return piecePosition.lap;
    }

    public int getCurrentPieceIndex() {
        return piecePosition.pieceIndex;
    }
    
    public double getInPieceDistance() {
        return piecePosition.inPieceDistance;
    }

    public double getAngle() {
        return angle;
    }

    public int getStartLaneIndex() {
        return piecePosition.lane.startLaneIndex;
    }

    public int getEndLaneIndex() {
        return piecePosition.lane.endLaneIndex;
    }

    public boolean checkCarColor(String color) {
        return carId.color.equals(color);
    }

}
