
package noobbot.model.json;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class Race {

    private static class RaceSession {
        public int laps;
        public int maxLapTimeMs;
        public int durationMs;
        public boolean quickRace;
    }

    private TrackInfo track;
    public List<Object> cars;
    public RaceSession raceSession;

    public Race(@JsonProperty(value = "track")
    TrackInfo track) {
        this.track = track;
    }

    public TrackInfo getTrackInfo() {
        return track;
    }

    public int getLapsQuantity() {
        return raceSession.laps;
    }

    public boolean isTestRace() {
        return raceSession.quickRace;
    }

    public boolean isQualifyingSession() {
        return raceSession.durationMs > 0;
    }

    public boolean isRaceSession() {
        return !isQualifyingSession() && !isTestRace();
    }

    public int getMaxLapTime() {
        return raceSession.maxLapTimeMs;
    }
}
