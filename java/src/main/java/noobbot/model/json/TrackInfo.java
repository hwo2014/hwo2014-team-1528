
package noobbot.model.json;

import java.util.List;

public class TrackInfo {
    public String id;
    public String name;
    public List<Piece> pieces;

    public List<Object> lanes;
    public Object startingPoint;

}
