
package noobbot.model.json;

import org.codehaus.jackson.annotate.JsonProperty;

public class Piece {
    public int length;
    @JsonProperty("switch")
    public boolean isSwitch;
    public int radius;
    public float angle;

    public boolean isStraight() {
        return radius == 0f;
    }

    public boolean isBendRight() {
        return isBend() && angle > 0;
    }

    public boolean isBendLeft() {
        return isBend() && angle < 0;
    }

    public boolean isBend() {
        return !isStraight();
    }
}
