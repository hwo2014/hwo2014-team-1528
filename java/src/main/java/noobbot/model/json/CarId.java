
package noobbot.model.json;

import org.codehaus.jackson.annotate.JsonProperty;

public class CarId {

    @JsonProperty
    String name;
    @JsonProperty
    String color;

    public String getColor() {
        return color;
    }
}
