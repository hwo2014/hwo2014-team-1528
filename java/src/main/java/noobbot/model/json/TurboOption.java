
package noobbot.model.json;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class TurboOption {

    private boolean mIsAvailable = true;
    private int mStartTick = -1;
    private int mEndTick = -1;
    private final int mDuration;
    private final double mFactor;

    public TurboOption(double factor, int duration) {
        mFactor = factor;
        mDuration = duration;
    }

    @JsonCreator
    private TurboOption(@JsonProperty("turboDurationMilliseconds")
    double durationInMillis, @JsonProperty("turboDurationTicks")
    int durationInTicks, @JsonProperty("turboFactor")
    double factor) {
        this(factor, durationInTicks);
    }

    public boolean isAvailable() {
        return mIsAvailable;
    }

    public void activate(int currentTick) {
        mIsAvailable = false;
        mStartTick = currentTick;
        mEndTick = currentTick + mDuration;
    }

    public boolean isActive(int gameTick) {
        return gameTick >= mStartTick && gameTick <= mEndTick;
    }

    public double getFactor() {
        return mFactor;
    }

    public int getDuration() {
        return mDuration;
    }

}
