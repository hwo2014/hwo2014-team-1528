
package noobbot;

import java.util.List;

import noobbot.model.json.CarPosition;
import noobbot.model.json.Piece;
import noobbot.model.json.TrackInfo;

public class CarCounter {

    public double getCarDistance(CarPosition carPosition, TrackInfo trackInfo) {
        int currentPieceIndex = carPosition.getCurrentPieceIndex();
        double inPieceDistance = carPosition.getInPieceDistance();
        List<Piece> pieces = trackInfo.pieces;
        double distance = 0;

        for (int i = 0; i < currentPieceIndex; i++) {
            distance = distance + calculatePieceLength(pieces.get(i));
        }

        return distance + inPieceDistance;
    }

    public static double calculatePieceLength(Piece piece) {
        if (piece.isStraight()) {
            return piece.length;
        } else {
            return calculateBendPieceLength(piece.angle, piece.radius);
        }
    }

    public static double calculateBendPieceLength(double angle, double radius) {
        return (Math.abs(angle) / 360) * 2 * Math.PI * radius;
    }
}
