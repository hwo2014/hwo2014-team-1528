package noobbot.msg.in;


import java.io.IOException;
import java.util.List;

import noobbot.model.json.CarId;
import noobbot.model.json.CarPosition;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

public class CarPositions {

    private static class CarPositionsInMsg extends InMsgBase {
        public List<CarPosition> data;
    }

    private CarPositionsInMsg message;
    
    private int mMyCarIndex = 0;
    
	private CarPositions(CarPositionsInMsg msg) {
        message = msg;
    }

    public static CarPositions fromJson(String jsonString)
			throws JsonParseException, IOException {

		JsonParser parser = new JsonFactory().createJsonParser(jsonString);
		ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(parser);
        CarPositionsInMsg msg = mapper.readValue(node, CarPositionsInMsg.class);
		return new CarPositions(msg);
	}

	private CarPosition guessMyPosition() {
		return message.data.get(mMyCarIndex);
	}

    public int getTick() {
        return message.gameTick;
    }

    public CarPosition getCarPosition(CarId car) {
        int initialIndex = mMyCarIndex;
        CarPosition position = guessMyPosition();
        while (!position.checkCarColor(car.getColor())) {
            mMyCarIndex += 1;
            mMyCarIndex %= message.data.size();
            if (mMyCarIndex == initialIndex) {
                throw new IllegalArgumentException("Car with color '" + car.getColor() + "' cannot be found!");
            }
            position = guessMyPosition();
        }
        return position;
    }

    public CarPosition getCarPositionByIndex(int i) {
        return message.data.get(mMyCarIndex);
    }

}
