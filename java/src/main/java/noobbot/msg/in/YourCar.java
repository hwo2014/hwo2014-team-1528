package noobbot.msg.in;

import java.io.IOException;

import noobbot.model.json.CarId;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;


public class YourCar {

    private static class YourCarInMsg extends InMsgBase {
        @JsonProperty
        CarId data;
    }
    
    public static CarId parse(String json) throws JsonParseException, IOException {
        JsonParser parser = new JsonFactory().createJsonParser(json);
        ObjectMapper mapper = new ObjectMapper();
        YourCarInMsg message = mapper.readValue(mapper.readTree(parser), YourCarInMsg.class);
        return message.data;
    }

}
