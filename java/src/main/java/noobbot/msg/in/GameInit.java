
package noobbot.msg.in;

import java.io.IOException;

import noobbot.model.json.Race;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

public class GameInit {

    private static class RaceData {
        public Race race;
    }

    private static class GameInitInMsg extends InMsgBase {
        public RaceData data;
    }

    public static Race parse(String gameInitString) throws JsonParseException,
            IOException {
        JsonParser parser = new JsonFactory().createJsonParser(gameInitString);
        ObjectMapper mapper = new ObjectMapper();
        GameInitInMsg message = mapper.readValue(mapper.readTree(parser), GameInitInMsg.class);
        return message.data.race;

    }

}
