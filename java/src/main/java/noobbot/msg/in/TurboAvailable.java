
package noobbot.msg.in;

import java.io.IOException;

import noobbot.model.json.TurboOption;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

public class TurboAvailable {

    private static class TurboAvailableInMsg extends InMsgBase {
        public TurboOption data;
    }

    public static TurboOption parse(String json) throws JsonParseException, IOException {
        JsonParser parser = new JsonFactory().createJsonParser(json);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(parser);
        TurboAvailableInMsg msg = mapper.readValue(node, TurboAvailableInMsg.class);
        return msg.data;
    }

}
