package noobbot.msg.out;

public interface OutMsgBase {

	public abstract String toJson();

	public abstract String msgType();

	public abstract Object msgData();

}