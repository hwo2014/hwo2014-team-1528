package noobbot.msg.out;

public class Join extends OutMessage implements JoinBase {
	public final String name;
	public final String key;

	public Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	public String msgType() {
		return "join";
	}
}