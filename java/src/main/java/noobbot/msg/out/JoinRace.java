package noobbot.msg.out;

public class JoinRace extends OutMessage implements JoinBase {

    @SuppressWarnings("unused")
	private static class BotId {
		public String name;
		public String key;
	}

	private JoinRace.BotId botId = new BotId();
	@SuppressWarnings("unused")
    private String trackName;
	@SuppressWarnings("unused")
	private int carCount;

	public JoinRace(String botName, String botKey, String trackName,
			int carCount) {
		botId.name = botName;
		botId.key = botKey;
		this.trackName = trackName;
		this.carCount = carCount;
	}

	@Override
	public String msgType() {
		return "joinRace";
	}

}