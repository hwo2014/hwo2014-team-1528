package noobbot.msg.out;

import com.google.gson.Gson;

public abstract class OutMessage implements OutMsgBase {

    @SuppressWarnings("unused")
    private static class MsgWrapper {
        public final String msgType;
        public final Object data;

        MsgWrapper(final String msgType, final Object data) {
            this.msgType = msgType;
            this.data = data;
        }

        public MsgWrapper(final OutMsgBase sendMsg) {
            this(sendMsg.msgType(), sendMsg.msgData());
        }
    }
    
	@Override
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	@Override
	public Object msgData() {
		return this;
	}

	@Override
	public abstract String msgType();
}