package noobbot.msg.out;

public class Turbo extends OutMessage {
    private String massage;

    public Turbo() {
        this("More power!");
    }
    
    public Turbo(String massage) {
        this.massage = massage;
    }

    @Override
    public Object msgData() {
        return massage;
    }

    @Override
    public String msgType() {
        return "turbo";
    }
}