
package noobbot.msg.out;

public class Throttle extends OutMessage {
    private double value;

    public Throttle(double value) {
        _setValue(value);
    }

    public Throttle() {
        this(0.0);
    }

    public void setValue(double value) {
        _setValue(value);
    }

    private final void _setValue(double value) {
        this.value = value;
    }
    
    @Override
    public Object msgData() {
        return value;
    }

    @Override
    public String msgType() {
        return "throttle";
    }
}
