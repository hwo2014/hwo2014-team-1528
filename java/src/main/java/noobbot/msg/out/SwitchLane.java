package noobbot.msg.out;

public class SwitchLane extends OutMessage {

	private String direction;

	public SwitchLane(final String direction) {
		this.direction = direction;
	}

	@Override
	public Object msgData() {
		return direction;
	}

	@Override
	public String msgType() {
		return "switchLane";
	}
}