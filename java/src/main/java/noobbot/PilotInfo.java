
package noobbot;

public class PilotInfo {
    private boolean isStart;
    private boolean isStraight;
    private boolean isBendLeft;
    private boolean isBendRight;
    private boolean beCareful;

    private boolean isNextTrackPartStraight;
    private boolean isNextTrackPartBendLeft;
    private boolean isNextTrackPartBendRight;

    private double angleSum;

    public boolean isBeCareful() {
        return beCareful;
    }

    public void setBeCareful(boolean beCareful) {
        this.beCareful = beCareful;
    }

    public double getAngleSum() {
        return angleSum;
    }

    public void setAngleSum(double angleSum) {
        this.angleSum = angleSum;
    }

    private int nextTrackPartFirstPieceIndex;
    private double length;

    public int getNextTrackPartFirstPieceIndex() {
        return nextTrackPartFirstPieceIndex;
    }

    public void setNextTrackPartFirstPieceIndex(int nextTrackPartFirstPieceIndex) {
        this.nextTrackPartFirstPieceIndex = nextTrackPartFirstPieceIndex;
    }

    public boolean isNextTrackPartStraight() {
        return isNextTrackPartStraight;
    }

    public void setNextTrackPartStraight(boolean isNextTrackPartStraight) {
        this.isNextTrackPartStraight = isNextTrackPartStraight;
    }

    public boolean isNextTrackPartBendLeft() {
        return isNextTrackPartBendLeft;
    }

    public void setNextTrackPartBendLeft(boolean isNextTrackPartBendLeft) {
        this.isNextTrackPartBendLeft = isNextTrackPartBendLeft;
    }

    public boolean isNextTrackPartBendRight() {
        return isNextTrackPartBendRight;
    }

    public void setNextTrackPartBendRight(boolean isNextTrackPartBendRight) {
        this.isNextTrackPartBendRight = isNextTrackPartBendRight;
    }

    public boolean isStraight() {
        return isStraight;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean isStart) {
        this.isStart = isStart;
    }

    public void setStraight(boolean isStraight) {
        this.isStraight = isStraight;
    }

    public boolean isBendLeft() {
        return isBendLeft;
    }

    public void setBendLeft(boolean isBendLeft) {
        this.isBendLeft = isBendLeft;
    }

    public boolean isBendRight() {
        return isBendRight;
    }

    public void setBendRight(boolean isBendRight) {
        this.isBendRight = isBendRight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        if (isStraight)
            return "Straight by " + length + " beCareful " + beCareful + " angle sum " + angleSum;

        if (isBendLeft)
            return "Bend Left by " + length + " beCareful " + beCareful + " angle sum " + angleSum;

        if (isBendRight)
            return "Bend Right by " + length + " beCareful " + beCareful + " angle sum " + angleSum;

        return super.toString();
    }
}
