
package nobbot;

import static org.fest.assertions.api.Assertions.assertThat;

import java.io.IOException;

import noobbot.model.json.TurboOption;
import noobbot.msg.in.TurboAvailable;

import org.codehaus.jackson.JsonParseException;
import org.junit.Before;
import org.junit.Test;

public class TurboOptionTests {

    private TurboOption turbo;

    @Before
    public void setup() throws JsonParseException, IOException {
        String json = "{\"msgType\":\"turboAvailable\",\"data\":{\"turboDurationMilliseconds\":500.0,\"turboDurationTicks\":30,\"turboFactor\":3.0},\"gameId\":\"65bbe88f-cef7-4d19-ab35-5c44105343a6\",\"gameTick\":601}";
        turbo = TurboAvailable.parse(json);
    }

    @Test
    public void testTurboIsAvailableByDefault() throws JsonParseException, IOException {
        assertThat(turbo.isAvailable()).isTrue();
    }

    @Test
    public void testTurboIsNotAvailableAfterActivation() throws JsonParseException, IOException {
        turbo.activate(0);
        assertThat(turbo.isAvailable()).isFalse();
    }

    @Test
    public void testTurboIsNotActiveByDefault() throws Exception {
        assertThat(turbo.isActive(0)).isFalse();
    }

    @Test
    public void testTurboIsActiveOneTickAfterActivation() throws Exception {
        turbo.activate(0);
        assertThat(turbo.isActive(1)).isTrue();
    }

    @Test
    public void testTurboRemainsActiveForDurationPeriod() throws Exception {
        turbo.activate(0);
        int lastTick = turbo.getDuration();
        assertThat(turbo.isActive(lastTick)).isTrue();
    }

    @Test
    public void testTurboGetsInactiveJustAfterDurationPeriodExpires() throws Exception {
        turbo.activate(0);
        int lastTick = turbo.getDuration();
        assertThat(turbo.isActive(lastTick + 1)).isFalse();
    }

    @Test
    public void testTurboIsActiveWithinDurationWhenStartIsInNonZeroTick() throws Exception {
        assertThat(turbo.isActive(0)).isFalse();
        int startTick = 10;
        assertThat(turbo.isActive(startTick - 1)).isFalse();
        turbo.activate(startTick);
        assertThat(turbo.isActive(startTick)).isTrue();
        int lastTick = startTick + turbo.getDuration();
        assertThat(turbo.isActive(lastTick)).isTrue();
        assertThat(turbo.isActive(lastTick + 1)).isFalse();
    }
}
