package nobbot;


import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;

import noobbot.model.json.CarId;
import noobbot.msg.in.CarPositions;
import noobbot.msg.in.YourCar;

import org.codehaus.jackson.JsonParseException;
import org.junit.Before;
import org.junit.Test;

public class CarPositionsTests {

    private CarId redCar;
    private CarId blueCar;
    private CarPositions position1;
    private CarPositions position2;

    @Before
    public void setup() throws JsonParseException, IOException {
        redCar = YourCar.parse("{\"msgType\":\"yourCar\",\"data\":{\"name\":\"LukaBeaver\",\"color\":\"red\"},\"gameId\":\"20abd1ee-b6ba-49f3-91af-f17fb86211b6\"}");
        blueCar = YourCar.parse("{\"msgType\":\"yourCar\",\"data\":{\"name\":\"AnotherBeaver\",\"color\":\"blue\"},\"gameId\":\"20abd1ee-b6ba-49f3-91af-f17fb86211b6\"}");
        position1 = CarPositions.fromJson("{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"LukaBeaver\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}},{\"id\":{\"name\":\"Another\",\"color\":\"blue\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":1,\"endLaneIndex\":1},\"lap\":0}}],\"gameId\":\"20abd1ee-b6ba-49f3-91af-f17fb86211b6\",\"gameTick\":1}");
        position2 = CarPositions.fromJson("{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Another\",\"color\":\"blue\"},\"angle\":1.0,\"piecePosition\":{\"pieceIndex\":1,\"inPieceDistance\":0.6,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":1},\"lap\":1}},{\"id\":{\"name\":\"LukaBeaver\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":3,\"inPieceDistance\":1.3,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"20abd1ee-b6ba-49f3-91af-f17fb86211b6\",\"gameTick\":1}");
    }
    
    @Test
    public void testFirstCarOnTheListIsProperlyAccessedViaColor() throws JsonParseException, IOException {
        assertThat(position1.getCarPosition(redCar)).isEqualTo(position1.getCarPositionByIndex(0));
    }
    
    @Test
    public void testSecondCarOnTheListIsProperlyAccessedViaColor() throws JsonParseException, IOException {
        assertThat(position1.getCarPosition(blueCar)).isEqualTo(position1.getCarPositionByIndex(1));
    }

    @Test
    public void testCarIsProperlyAccessedViaColorWhenItsPositionInTheListChanges() throws JsonParseException, IOException {
        assertThat(position1.getCarPosition(redCar)).isEqualTo(position1.getCarPositionByIndex(0));
        assertThat(position2.getCarPosition(redCar)).isEqualTo(position2.getCarPositionByIndex(1));
    }

    @Test
    public void testCarIsProperlyAccessedViaColorWhenItsPositionInTheListChangesToTheFirst() throws JsonParseException, IOException {
        assertThat(position1.getCarPosition(blueCar)).isEqualTo(position1.getCarPositionByIndex(1));
        assertThat(position2.getCarPosition(blueCar)).isEqualTo(position2.getCarPositionByIndex(0));
    }
    
    @Test
    public void testExceptionIsThrownWhenCarCnnotBeFound() throws JsonParseException, IOException {
        CarId myCar = YourCar.parse("{\"msgType\":\"yourCar\",\"data\":{\"name\":\"LukaBeaver\",\"color\":\"yellow\"},\"gameId\":\"20abd1ee-b6ba-49f3-91af-f17fb86211b6\"}");
        try {
            position1.getCarPosition(myCar);
            fail("Should not reach this point");
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("Car with color 'yellow' cannot be found!");
        }
    }
}
