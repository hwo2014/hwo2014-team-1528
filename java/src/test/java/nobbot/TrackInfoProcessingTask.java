
package nobbot;

import static org.fest.assertions.api.Assertions.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import junit.framework.TestCase;
import noobbot.CarModel;
import noobbot.model.json.CarPosition;
import noobbot.model.json.Race;
import noobbot.model.json.TrackInfo;
import noobbot.msg.in.CarPositions;
import noobbot.msg.in.GameInit;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;

public class TrackInfoProcessingTask extends TestCase {

    @Test
    public void test() throws JsonParseException, IOException {
        TrackInfo track = getTrackInfo();
        assertThat(track).isNotNull();
        assertThat(track.pieces.size()).isEqualTo(40);

        assertThat(track.pieces.get(0).isStraight()).isTrue();
        assertThat(track.pieces.get(4).isBendRight()).isTrue();
    }

    private TrackInfo getTrackInfo() throws IOException, JsonParseException,
            JsonProcessingException, JsonMappingException {
        String gameInitString = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"LukaBeaver\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"c6421d68-1e88-4e60-a863-064d7ec0e638\"}";

        Race race = GameInit.parse(gameInitString);

        assertThat(race).isNotNull();

        TrackInfo track = race.getTrackInfo();
        return track;
    }

    @Test
    public void test2() throws JsonParseException, JsonMappingException,
            JsonProcessingException, IOException {
        CarModel car = new CarModel(getTrackInfo());
        assertThat(car.updatePosition(0, 0, 0.13, 0)).isEqualTo(1);
        assertThat(car.getDistance()).isEqualTo(0.13);
        assertThat(car.getVelocity()).isEqualTo(0.13);
        assertThat(car.getAvgVelocity()).isEqualTo(0.13);
        assertThat(car.getAcceleration()).isEqualTo(0.13);

        assertThat(car.updatePosition(0, 0, 0.3874, 0)).isEqualTo(2);
        assertThat(car.getDistance()).isEqualTo(0.3874);
        assertThat(car.getVelocity()).isEqualTo(0.2574);
        assertThat(car.getAvgVelocity()).isEqualTo(0.1937);
        assertThat(car.getAcceleration()).isEqualTo(0.1274);

        assertThat(car.updatePosition(0, 0, 0.769652, 0)).isEqualTo(3);
        assertThat(car.getDistance()).isEqualTo(0.769652);
        assertThat(car.getVelocity()).isEqualTo(0.382252);
        assertThat(car.getAvgVelocity()).isEqualTo(0.769652 / 3);
        assertThat(car.getAcceleration()).isEqualTo(0.382252 - 0.2574);

        assertThat(car.updatePosition(0, 1, 0.1, 0)).isEqualTo(4);
        assertThat(car.getDistance()).isEqualTo(100.1);

        assertThat(car.updatePosition(0, 3, 0.123, 0)).isEqualTo(5);
        assertThat(car.getDistance()).isEqualTo(300.123);

    }

    @Test
    public void testBendDetection() throws JsonParseException, JsonMappingException,
            JsonProcessingException, IOException {
        TrackInfo trackInfo = getTrackInfo();
        CarModel car = new CarModel(trackInfo);

        int pieceIndex = 0;
        int bendIndex = 0;
        while (trackInfo.pieces.get(bendIndex).isStraight()) {
            bendIndex++;
        }

        assertThat(car.updatePosition(0, 0, 0.13, 0)).isEqualTo(1);

        assertThat(car.getDistance()).isEqualTo(0.13);
        assertThat(car.getVelocity()).isEqualTo(0.13);
        assertThat(car.getAvgVelocity()).isEqualTo(0.13);
        assertThat(car.getAcceleration()).isEqualTo(0.13);

        assertThat(car.updatePosition(0, 0, 0.3874, 0)).isEqualTo(2);
        assertThat(car.getDistance()).isEqualTo(0.3874);
        assertThat(car.getVelocity()).isEqualTo(0.2574);
        assertThat(car.getAvgVelocity()).isEqualTo(0.1937);
        assertThat(car.getAcceleration()).isEqualTo(0.1274);

        assertThat(car.updatePosition(0, 0, 0.769652, 0)).isEqualTo(3);
        assertThat(car.getDistance()).isEqualTo(0.769652);
        assertThat(car.getVelocity()).isEqualTo(0.382252);
        assertThat(car.getAvgVelocity()).isEqualTo(0.769652 / 3);
        assertThat(car.getAcceleration()).isEqualTo(0.382252 - 0.2574);

        assertThat(car.updatePosition(0, 1, 0.1, 0)).isEqualTo(4);
        assertThat(car.getDistance()).isEqualTo(100.1);

        assertThat(car.updatePosition(0, 3, 0.123, 0)).isEqualTo(5);
        assertThat(car.getDistance()).isEqualTo(300.123);

    }

    @Test
    public void test3() throws JsonParseException, JsonMappingException,
            JsonProcessingException, IOException {

        CarModel car = new CarModel(getTrackInfo());
        Path path = Paths.get("..").resolve("log.txt");
        try (BufferedReader reader = Files.newBufferedReader(path,
                Charset.forName("UTF-8"))) {
            String line;
            double distance = -1;
            while ((line = reader.readLine()) != null) {
                CarPositions message = CarPositions.fromJson(line);
                CarPosition position = message.getCarPositionByIndex(0);
                car.updatePosition(position.getCurrentLap(),
                        position.getCurrentPieceIndex(),
                        position.getInPieceDistance(), position.getAngle());
                if (car.getDistance() != distance) {
                    System.out.println("tick: " + message.getTick() + "\tlap: " + position.getCurrentLap() + "\tpiece: "
                            + position.getCurrentPieceIndex() + "\ts: "
                            + car.getDistance() + "\tv: " + car.getVelocity()
                            + "\ta: " + car.getAcceleration() + "\tangle: "
                            + car.getAngle());
                    distance = car.getDistance();
                }
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

}
