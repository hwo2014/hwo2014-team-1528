
package nobbot;

import static org.fest.assertions.api.Assertions.assertThat;

import java.io.IOException;

import noobbot.model.json.TurboOption;
import noobbot.msg.in.TurboAvailable;

import org.codehaus.jackson.JsonParseException;
import org.junit.Test;

public class TurboAvailableParsingTest {

    String json = "{\"msgType\":\"turboAvailable\",\"data\":{\"turboDurationMilliseconds\":500.0,\"turboDurationTicks\":30,\"turboFactor\":3.0},\"gameId\":\"65bbe88f-cef7-4d19-ab35-5c44105343a6\",\"gameTick\":601}";
    String jsonShort = "{\"msgType\":\"turboAvailable\",\"data\":{\"turboDurationMilliseconds\":500.0,\"turboDurationTicks\":30,\"turboFactor\":3.0}}";

    @Test
    public void testFactorIsCorrect() throws JsonParseException, IOException {
        TurboOption turbo = TurboAvailable.parse(json);
        assertThat(turbo.getFactor()).isEqualTo(3.0);
    }

    @Test
    public void testDurationIsCorrect() throws JsonParseException, IOException {
        TurboOption turbo = TurboAvailable.parse(json);
        assertThat(turbo.getDuration()).isEqualTo(30);
    }

    @Test
    public void testShortJsonIsAcceptable() throws JsonParseException, IOException {
        TurboOption turbo = TurboAvailable.parse(jsonShort);
        assertThat(turbo.getDuration()).isEqualTo(30);
        assertThat(turbo.getFactor()).isEqualTo(3.0);
    }

}
